"""
    This type of algorithm have two obligatory functions:

        *initial_allocation*: invoked at the start of the simulation

        *run* invoked according to the assigned temporal distribution.

"""

from yafs_ext.placement import Placement


class CloudPlacement(Placement):
    """
    This implementation locates the services of the application in the cheapest cloud regardless of where the sources or sinks are located.

    It only runs once, in the initialization.

    """
    def initial_allocation(self, sim, app_name):
        #We find the ID-nodo/resource
        value = {"mytag": "cloud"} # or whatever tag

        id_cluster = sim.topology.find_IDs(value)
        print "id_cluster:"
        print id_cluster

        app = sim.apps[app_name]
        services = app.services

        for module in services:
            if module in self.scaleServices:
                for rep in range(0, self.scaleServices[module]):
                    idDES = sim.deploy_module(app_name,module,services[module],id_cluster)

    #end function

class TestPlacement (Placement):

    def initial_allocation (self, sim, appName):

        #value = {"id":2}
        #nodeIdList = sim.topology.find_IDs (value)

        app = sim.apps[appName]
        serviceList = app.services

        for nodeId in sim.topology.nodeAttributes:
            for serviceName in serviceList:
                if  serviceName == "EdgeService{0:d}".format (nodeId):
                    idDES = sim.deploy_edge_module (appName, serviceName, nodeId)
                    break
                if  serviceName == "CloudService{0:d}".format (nodeId):
                    idDES = sim.deploy_edge_module (appName, serviceName, nodeId)
                    break
                if serviceName == "RequesterService{0:d}".format (nodeId):
                    idDES = sim.deploy_edge_module (appName, serviceName, nodeId)
                    break
                if serviceName == "SourceService{0:d}".format (nodeId):
                    idDES = sim.deploy_edge_module (appName, serviceName, nodeId)
                    break
                if serviceName == "PsanService{0:d}".format (nodeId):
                    idDES = sim.deploy_edge_module (appName, serviceName, nodeId)


        # app = sim.apps[app_name]
        # services = app.services
        #
        # for module in services:
        #     if module in self.scaleSerices:
        #         for nodeId in nodeIdList:
        #             idDES = sim.deploy_edge_module (app_name, module, services[module], nodeId)
