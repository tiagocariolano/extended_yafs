"""

    Created on Mon Out 22 12:00:00 2018

    @author: tiago cariolano
    @citation: T. C. S. XAVIER

"""

from math import sqrt
from edge_node import EdgeNode
from request import Request
from util import *

class CloudNode:
	CPU_RATE = 8 * GB

	def __init__(self, env, interface):
		self.env = env
		self.interface = interface
		self.process = None
		self.edge_node = None
		self.data_types = []

	def set_edge_node (self, edge_node):
		self.edge_node = edge_node

	def set_data_types (self, data_types):
		self.data_types = data_types

	def run (self):
		self.process = self.env.process (self.operation_thread ())

	def operation_thread (self):
		while True:
			try:
				yield self.env.timeout (SIMULATION_TIME)

			except simpy.Interrupt as interrupt:
				event = interrupt.cause
				if event.type == event.ARRIVED_MSG:
					request = event.obj
					log.log ("Cloud:\n")
					log.log ("\t", request, "-> ", request.tStart)
					self.env.timeout (float (request.cycle) / self.CPU_RATE)

	def send_message (self, neighbor, msg):
		ev = Event (Event.ARRIVED_MSG, msg)
		self.interface.send_message (ev, neighbor)
