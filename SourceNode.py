"""

    Created on Mon Out 22 12:00:00 2018

    @author: tiago cariolano
    @citation: T. C. S. XAVIER

"""

import numpy as np
from request import Request
from util import *
from data_type import DataType

class SourceNode:

    def __init__(self, env, interface):
        self.env = env
        self.interface = interface
        self.type = "REQUESTER"
        self.edge_node = None
        self.process = None

    def set_edge_node (self, edge_node):
        self.edge_node = edge_node
    
    def set_data_type (self, data_type):
        self.data_type = data_type

    def run (self):
        self.process = self.env.process (self.operation_thread ())

    def operation_thread (self):
        log.log ("Source:")
        
        request = Request (100000,
                            1000000,
                            1000000,
                            self.data_type)
        request.set_t_start (self.env.now)
        log.log ("\t", request, "-> ", request.tStart)
        self.send_message (self.edge_node, request)

        yield self.env.event ()

    def send_message (self, neighbor, msg):
        ev = Event (Event.ARRIVED_MSG, msg)
        self.interface.send_message (ev, neighbor)
