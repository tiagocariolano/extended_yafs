"""

    Created on Mon Out 22 12:00:00 2018

    @author: tiago cariolano
    @citation: T. C. S. XAVIER

"""

from math import sqrt
from request import *
from util import *

class EdgeNode:
	"""
		This class implements a edge node behavior (operation thread).
	"""
	MASTER = 'MASTER'						# Indicates Edge Node is a master
	SLAVE = 'SLAVE'							# Indicates Edge Node is a slave

	def __init__(self, env, interface, id, type, posX, posY, cpuRate, totalRAM, totalStorage):
		"""
			EdgeNode constructor method.
		"""
		self.env = env
		self.interface = interface
		self.id = id
		self.process = None
		self.psan = None
		self.cloud = None
		self.requester = None
		self.cpuRate = cpuRate
		self.totalRAM = self.RAM = totalRAM
		self.dtList = []
		self.type = "MASTER"


		log.log ( 'Init Edge Node')
		print self

	def add_data_type (self, dt):
		self.dtList.append (dt)

	def set_psan_node (self, psan):
		self.psan = psan
	
	def set_cloud_node (self, cloud):
		self.cloud = cloud

	def set_requester_node (self, requester):
		self.requester = requester

	def run (self):
		self.process = self.env.process (self.operation_thread ())

	def operation_thread (self):
		while True:
			try:
				yield self.env.timeout (SIMULATION_TIME)

			except simpy.Interrupt as interrupt:
			 	event = interrupt.cause
				log.log(self)
				if event.type == Event.ARRIVED_MSG:
					request = event.obj
					log.log ("=> Receive request")
					self.send_message (self.psan, RequestData())

	def send_message (self, neighbor, msg):
		"""
			Send a messsage to a neighbor edge node. Create event ARRIVED_MSG
			and generate interruption in neighbor edge node.

			Args:
				neighbor: a neighbor edge node

				msg: message to be sent
		"""
		ev = Event (Event.ARRIVED_MSG, msg)
		self.interface.send_message (ev, neighbor)

	def __str__ (self):
		"""
			Print resources info about edge node
		"""
		GB = GHz = 10 ** 9
		MB = MHz = 10 ** 6
		out = "{:5s} {:02d}: {:08.4f}, {:010.4f}".format ("EN", self.id,
																	float (self.cpuRate) / GHz, float (self.RAM) / MB)
		return out
