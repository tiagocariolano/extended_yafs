"""

    Created on Mon Out 22 12:00:00 2018

    @author: tiago cariolano
    @citation: T. C. S. XAVIER

"""

import random
import numpy as np
import argparse
import time
import simpy

from yafs_ext.core import Sim
from yafs_ext.application import Application,Message,UpLayerInterface
from yafs_ext.population import *
from yafs_ext.topology import Topology
from simpleSelection import MinimunPath
from simplePlacement import TestPlacement
from yafs_ext.stats import Stats
from yafs_ext.distribution import deterministicDistribution
from yafs_ext.utils import fractional_selectivity

from edge_node import EdgeNode
from cloud_node import CloudNode
from psan import PSAN
from data_type import DataType
from request import Request
from SourceNode import SourceNode
from util import *

RANDOM_SEED = 1
SIMULATION_TIME = 1000

GB = GHz = 10 ** 9
MB = MHz = 10 ** 6
KB = KHz = 10 ** 3
LIGHT_SPEED = 3 * 10 ** 5 # km/s
NANOSEC = 10 ** -9
DEFAULT_CABLE_DELAY  = 5 * NANOSEC * 10 ** 3# 5nS/m == 5000ns /km
propagation_delay = 0
BW = 1 * 10 ** 3
IPT = 2 #GHz
NEN = 2
NPSANBYEN = 1

RQ_CYCLE_DEF = 100 * GHz
RQ_RAM_DEF = 20 * MB
RQ_STORAGE_DEF = 0
RQ_PRICE_DEF = 1
RQ_UPDATE_DEF = False

class Id:
    def __init__ (self):
        self.id = -1
    def next (self):
        self.id += 1
        return self.id
id = Id ()

def create_json_topology():

    # esse env e usado em cada edge node e no yafs_ext
    env = simpy.Environment ()

    # Confs
    a = Application(name="AppTeste")
    placement = TestPlacement("Teste") # it defines the deployed rules: module-device
    pop = TestPopulation("Teste")
    selectorPath = MinimunPath()
    topology_json = {}
    topology_json["entity"] = []
    topology_json["link"] = []
    dt1 = DataType ("DT1")
    
    # Entities

    requesterId = id.next ()
    rqJson  = {"id": requesterId, "model": "edge",  "IPT": IPT * GHz, "RAM": 4 * GB,"COST": 3,"WATT":100.0}
    rqModName = "RequesterService" + str (requesterId)
    requesterModule = {rqModName: {"Type":Application.TYPE_EDGE}}
    intfRq = UpLayerInterface (a.upLayerAddresser, rqModName)
    rq = SourceNode (env, intfRq)
    rq.set_data_type (dt1)
    rq.run ()
    intfRq.set_up_layer_comm (rq.process, UpLayerInterface.EVENT)
    a.set_modules ([requesterModule])
    a.add_service_edge_module (rqModName, rq, intfRq)

    cloudId = id.next ()
    cnJson  = {"id": cloudId, "model": "edge",  "IPT": IPT * GHz, "RAM": 4 * GB,"COST": 3,"WATT":100.0}
    cnModName = "CloudService" + str (cloudId)
    cloudModule =  {cnModName: {"Type":Application.TYPE_EDGE}}
    intfCn = UpLayerInterface (a.upLayerAddresser, cnModName)
    cn = CloudNode (env, intfCn)
    cn.set_data_types([dt1])
    cn.run ()
    intfCn.set_up_layer_comm (cn.process, UpLayerInterface.EVENT)
    a.set_modules ([cloudModule])
    a.add_service_edge_module (cnModName, cn, intfCn)

    enId = id.next ()
    enJson = {"id": enId, "model": "edge",  "IPT": IPT * GHz, "RAM": 4 * GB,"COST": 3,"WATT":40.0}
    enModName  = "EdgeService{0:d}".format (enId)
    enModule = {enModName: {"Type":Application.TYPE_EDGE}}
    intfEn = UpLayerInterface (a.upLayerAddresser, enModName)
    en = EdgeNode (env, intfEn, enId, EdgeNode.MASTER, 0, 0, \
                    enJson["IPT"], \
                    enJson["RAM"], \
                    500 * GB)
    en.add_data_type (dt1)
    en.run ()
    intfEn.set_up_layer_comm (en.process, UpLayerInterface.EVENT)
    a.set_modules ([enModule])
    a.add_service_edge_module (enModName, en, intfEn)

    psanId = id.next ()
    psanJson = {"id": psanId, "model": "edge",  "IPT": 10 * KHz, "RAM": 16 * MB, "COST": 3,"WATT":0.5}
    psanModName = "PsanService{0:d}".format (psanId)
    psanModule = {psanModName: {"Type":Application.TYPE_EDGE}}
    intfPsan = UpLayerInterface (a.upLayerAddresser, psanModName)
    psan = PSAN (   env, intfPsan, dt1)
    psan.run ()
    intfPsan.set_up_layer_comm (psan.process, UpLayerInterface.EVENT)
    a.set_modules ([psanModule])
    a.add_service_edge_module (psanModName, psan, intfPsan)

    topology_json["entity"] = [rqJson] + [cnJson] + [enJson] + [psanJson]

    ##### Topology

    enRqLinkJson = {"s": requesterId, "d": enId, "BW": BW, "PR": propagation_delay}
    enCnLinkJson = {"s": cloudId, "d": enId, "BW": BW, "PR": propagation_delay}
    enPsanLinkJson = {"s": psanId, "d": enId, "BW": BW, "PR": propagation_delay}

    topology_json["link"] += [enRqLinkJson]
    topology_json["link"] += [enCnLinkJson]
    topology_json["link"] += [enPsanLinkJson]

    en.set_requester_node (rq)
    rq.set_edge_node (en)
    en.set_cloud_node (cn)
    cn.set_edge_node (en)
    en.set_psan_node (psan)
    psan.set_edge_node (en)
    

    return env, topology_json, a, placement, pop, selectorPath, en, cn

# @profile
def main(simulated_time):

    random.seed(RANDOM_SEED)
    np.random.seed (RANDOM_SEED)

    """
    TOPOLOGY from a json
    """
    t = Topology()
    env, t_json, app, placement, pop, selectorPath, enList, cloud = create_json_topology()
    t.load(t_json)
    t.write("network.gexf")

    """
    SIMULATION ENGINE
    """

    stop_time = simulated_time
    s = Sim(t, env=env, default_results_path="Results")
    s.deploy_app(app, placement, pop, selectorPath)

    s.run(stop_time,show_progress_monitor=False)

    s.draw_allocated_topology() # for debugging

    return s, enList, cloud

if __name__ == '__main__':
    import logging.config
    import os

    print os.getcwd()+'/logging.ini'
    logging.config.fileConfig(os.getcwd()+'/logging.ini')

    start_time = time.time()
    s, enList, cloud = main(simulated_time=SIMULATION_TIME)
