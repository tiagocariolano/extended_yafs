"""

    Created on Mon Out 22 12:00:00 2018

    @author: tiago cariolano
    @citation: T. C. S. XAVIER

"""

import random
import numpy as np
from util import *

class RequestData:
	def __init__ (self, payload= 1 * KB):
		self.payload = payload

class Request:

	def __init__(self, cycle, RAM, storage, dt, payload=1 * KB):
		self.tStart = 0
		self.tFinal = 0
		self.cycle = cycle
		self.RAM = RAM
		self.storage = storage
		self.dt = dt
		self.payload = payload

	def set_t_start (self, tStart):
		self.tStart = tStart
		self.t = tStart

	def set_t_final (self, tFinal):
		self.tFinal =tFinal

	def __str__ (self):
		GB = GHz = 10 ** 9
		MB = MHz = 10 ** 6
		return "{:7s}: {:08.4f}, {:010.4f}, {:08.4f}, {:6s}".format (
										"Request", float (self.cycle) / MHz, float (self.RAM) / MB,
										float (self.storage) / GB, self.dt.type)
