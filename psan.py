"""

    Created on Mon Out 22 12:00:00 2018

    @author: tiago cariolano
    @citation: T. C. S. XAVIER

"""

import numpy as np
from request import Request
from data_type import DataType
from util import *

class Data:
    def __init__ (self, value, time, payload=1 * KB):
        self.value = value
        self.time = time
        self.cycle = 1
        self.RAM = 8
        self.payload = payload

class PSAN: # This is the "physical sensor and actuator node", i.e. the "thing node"

    def __init__(self, env, interface, dt):
        self.interface = interface
        self.env = env
        self.dt = dt

    def set_edge_node (self, edge_node):
        self.edge_node = edge_node

    def run (self):
        self.process = self.env.process (self.operation_thread ())

    def operation_thread (self):
        while True:
            try:
            	yield self.env.timeout (SIMULATION_TIME)

            except simpy.Interrupt as interrupt:
             	event = interrupt.cause
                data_request = event.obj
                data = Data (np.random.randint (0, 100), self.env.now)
                log.log(self)
                log.log ("=> Receive request")
                self.send_message (self.edge_node, data)

    def send_message (self, neighbor, msg):
        ev = Event (Event.ARRIVED_DATA, msg)
        self.interface.send_message (ev, neighbor)

    def __str__ (self):
        return "PSAN "
