"""

    Created on Mon Out 22 12:00:00 2018

    @author: tiago cariolano
    @citation: T. C. S. XAVIER

"""

import random
import simpy

class Event:
	FINISHED_VN = 'FINISHED_VN'
	ARRIVED_MSG = 'ARRIVED_MSG'
	ARRIVED_DATA = 'ARRIVED_DATA'

	def __init__ (self, type, obj):
		self.type = type
		self.obj = obj

class CustomRandom:
	def __init__ (self, seed):
		self.seed = seed
		random.seed (seed)

	def gauss (self, mu, sd):
		#random.seed(self.seed)
		return random.gauss (mu, sd)

	def randint (self, low, high):
		#random.seed (self.seed)
		return random.randint (low, high)

	def choice (self, seq):
		#random.seed (self.seed)
		return random.choice (seq)

	def random (self):
		#random.seed (self.seed)
		return random.random ()

class Log:
	def __init__ (self,log_path="log.txt", debug =True, debug_log=False):
		self._debug = debug
		self._debug_log = debug_log
		self._f = open (log_path, "w")
		self.c = 0

	def log (self, *str_list):
		str_file = ""
		if self._debug or self._debug_log:
			for s in str_list:
				str_file += s.__str__ ()
		if self._debug:
			print str_file
		if self._debug_log:
			self._f.write (str_file + "\n")

	def error (self, str):
		print "Error: " + str + "."
		exit ()

	def count (self):
		self.c = self.c + 1
		if self.c % 100 == 0:
			print "Count: ", self.c

	def __del__ (self):
		self._f.close ()

## enviroment of simpy to control some events
#env = simpy.Environment ()

GB = GHz = 10 ** 9
MB = MHz = 10 ** 6
KB = KHz = 10 ** 3

## time max of simulation
SIMULATION_TIME = 1000000		# Simulation time

## seed and random object
SEED = 10
rnd = CustomRandom (SEED)

## Log output
log = Log ("output.txt", debug=True, debug_log=False)

DEBUG = False
STEP_BY_STEP = False
